#!/usr/bin/python3

# arguments:
# <file> <.torrent> <category>

import sys
import os
from qbittorrent import Client

#arguments
targetFile = sys.argv[1]
torrentFile = sys.argv[2]
category = sys.argv[3]
targetPath = os.path.split(targetFile)

#connection info
clientIP = 'http://127.0.0.1:8080/'
qbUsername = 'admin'
qbPassword = 'admin'

qb = Client(clientIP)
print(" --- Logging in to qBittorrent WebUI ---")
result = qb.login(username=qbUsername, password=qbPassword)
version = qb.qbittorrent_version
print(" --- qBittorrent version {} ---".format(version))
print(" --- Result from logging in was {} ---".format(("Success" if result == None else "Fail")))
openTorrentFile = open(torrentFile, 'rb')
print(" --- Adding torrent file {} ---".format(torrentFile))
qb.download_from_file(openTorrentFile, savepath=targetPath[0])
torrents = qb.torrents()
for item in torrents:
    if item['name'] == targetPath[1]:
        torHash = item['hash']
        break
if torHash != None:
    print(" --- Setting torrent category to {} ---".format(category))
    qb.set_category(torHash, category)
